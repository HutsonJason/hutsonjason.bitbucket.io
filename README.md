# README #

Format a csv file to a json format that is easily read in Gamemaker Studio 2.

The format I use in game is a map key and value. Everything in the first column are the map keys. Everything in the second are the key values.

This map can also setup as a map within a map. The map key title is actually the map key for the entire csv output.
More titles can be added in the html file as needed. Multiple maps can then be combined manually in a final json file as needed.

Two space indentation is also being using in the json string, and can be changed as needed.