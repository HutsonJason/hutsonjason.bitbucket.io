/*
Format a csv file to a json format that is easily read in Gamemaker Studio 2.
The format I use in game is a map key and value. Everything in the first column
are the map keys. Everything in the second are the key values. This map is also
setup as a map within a map. The map key title is actually the map key for the
entire csv output. More titles can be added in the html file as needed. Multiple
maps can then be combined manually in a final json file as needed.
Two space indentation is also being using in the json string, and can be changed
as needed.
*/

function convert(){
	var input = document.getElementById('inputBox').value; // Get the inputbox value.
	var lines = input.split('\n'); // Split the input data into an array of each line.
	var array = []; // Create and array to hold the values.
	var headerCheck = document.getElementById('headerRowCheck'); // Get the header row check box value.
	var arrayLength = 0; // Create array length var.
	var arrayStart = 0; // Array starting row.
	
	// Check if header row is checked.
	if (headerCheck.checked){
		arrayLength = lines.length - 1; // Set the array length to 1 less.
		arrayStart = 1; // Set the array start position to the second row (1), so it skips the header.
	}
	// If header row is not checked.
	else{
		arrayLength = lines.length; // Make the array the full length.
		arrayStart = 0; // Start the array at the first row (0), so it includes the header.
	}
	
	// Create a 2D array to hold the key and value.
	for (var i = 0; i < arrayLength; i++){
		array[i] = new Array(2);
	}
	
	
	// Split each line at the first comma into the key and value.
	/* It's only split at the first comma because we don't have any
		in our key column, and we have strings in the value column
		that may have commas in them.
		
		We subtract arrayStart from lines length to see how many rows.
		We add arrayStart to each line array we're accessing, to make
		sure we're on the right row. */
	for (var i = 0; i < lines.length - arrayStart; i++){
		array[i][0] = lines[i + arrayStart].substring(0,lines[i + arrayStart].indexOf(',')); // Set the key.
		array[i][1] = lines[i + arrayStart].substring(lines[i + arrayStart].indexOf(',')+1); // Set the value.
	}
	
	var addTitleOption = document.getElementById('addMapKeyTitleCheck'); // Get the add map key title check box value.
	var titleOption = document.getElementById('mapKeySelect'); // Get the map key title option.
	var strTitle = titleOption.options[titleOption.selectedIndex].text; // Create the string of the title option.
	var numberStringOption = document.getElementById('numberAsStringCheck'); // Get number as string check box value.
	var noQuotesOption = document.getElementById('noQuotesCheck'); // Get don't add quotes if already found value.
	var tab = '  '; // Tab space.
	var json = '{\n'; // Add the opening bracket.
	
	// Add map key title if checked.
	if (addTitleOption.checked) {
		json += '  "' + strTitle + '": {\n'; // Add the map key title.
		tab = '    '; // Increase the tab space to 4.
	}
	
	// Format each row.
	for (var i = 0; i < array.length; i++){
		// Check if key is a number, or if it should be a string.
		if (numberStringOption.checked || isNaN(array[i][0])){
			json += tab + '"' + array[i][0] + '": '; // If not a number, or should be a string, add quotes and formatting.
		}
		else{
			json += tab + array[i][0] + ': '; // If a number, and should not be a string, just add formatting.
		}
		
		// Check if each value is a number, or if it should be a string.
		if (numberStringOption.checked || isNaN(array[i][1])){
			// Check if a value already has quotes around it first.
			if (noQuotesCheck.checked && array[i][1].length > 1 && array[i][1].charAt(0) === '"' && array[i][1].charAt(array[i][1].length - 1) === '"'){
				json += array[i][1]; // If value is already surrounded by quotes, just add value.
			}
			else{
				json += '"' + array[i][1] + '"'; // If not a number, or should be a string, add quotes.
			}
		}
		else{
			json += array[i][1]; // If a number, and should not be a string, just add value.
		}
		
		// If not the last row, add comma and new line.
		if (i !== array.length - 1){
			json += ',\n';
		}
	}
	
	// Add map key title closing bracket if added.
	if (addTitleOption.checked) {
		json += '\n  }';
	}
	json += '\n}'; // Add the final closing bracket.
	document.getElementById('outputBox').value = json; // Set the outputBox to final json value.
}